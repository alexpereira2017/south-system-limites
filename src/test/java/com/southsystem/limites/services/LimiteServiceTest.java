package com.southsystem.limites.services;

import com.southsystem.limites.domain.LimiteParametro;
import com.southsystem.limites.dto.CustomMessage;
import com.southsystem.limites.dto.LimiteClienteMessage;
import com.southsystem.limites.service.LimiteService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

@ExtendWith(MockitoExtension.class)
public class LimiteServiceTest {

    @InjectMocks
    public LimiteService limiteService;

    @Test
    public void deve_calcular_limites_com_sucesso() {
        CustomMessage customMessage = new CustomMessage(1,5);
        List<LimiteParametro> parametros = new ArrayList<>();
        parametros.add(new LimiteParametro(4,6,150,300));

        final LimiteClienteMessage limites = limiteService.calcular(customMessage, parametros);

        Assert.assertThat(limites.getLimiteCartao(), is(150D));
        Assert.assertThat(limites.getLimiteChequeEspecial(), is(300D));
    }
}
