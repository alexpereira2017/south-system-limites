package com.southsystem.limites;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LimitesApplication {

    public static void main(String[] args) {
        SpringApplication.run(LimitesApplication.class, args);
    }

}
