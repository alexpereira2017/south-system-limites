package com.southsystem.limites.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class CustomMessage {
    private final int id;
    private final int escore;

    public CustomMessage(@JsonProperty("id") int id,
                         @JsonProperty("escore") int escore) {
        this.id = id;
        this.escore = escore;
    }
}
