package com.southsystem.limites.consumer;

import com.southsystem.limites.dto.CustomMessage;
import com.southsystem.limites.service.LimiteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitQMConsumer {

    @Value("${rabbitmq.queue.out}")
    String queueName;

    @Autowired
    private LimiteService limiteService;

    @RabbitListener(queues = "${rabbitmq.queue.in}")
    public void listen(final CustomMessage in) {
        log.info("Message read from myQueue : " + in);

        limiteService.processar(in);

        // Sleep para simular um processamento mais demorado
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
