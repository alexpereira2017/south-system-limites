package com.southsystem.limites.service;

import com.southsystem.limites.domain.LimiteParametro;
import com.southsystem.limites.dto.CustomMessage;
import com.southsystem.limites.dto.LimiteClienteMessage;
import com.southsystem.limites.producer.RabbitMQProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LimiteService {

    @Autowired
    private RabbitMQProducer messageProducer;

    public void processar(CustomMessage in) {
        final LimiteClienteMessage novoLimite = calcular(in, getParametos());
        messageProducer.produce(novoLimite);
    }

    public LimiteClienteMessage calcular(CustomMessage in, List<LimiteParametro> parametros) {
        LimiteClienteMessage limiteClienteMessage = new LimiteClienteMessage();
        limiteClienteMessage.setConta_id(in.getId());
        limiteClienteMessage.setEscore(in.getEscore());

        for(LimiteParametro limite : parametros) {
            if (in.getEscore() >= limite.getEscoreMinimo() && in.getEscore() <= limite.getEscoreMaximo()) {
                limiteClienteMessage.setLimiteCartao(limite.getLimiteCartao());
                limiteClienteMessage.setLimiteChequeEspecial(limite.getLimiteChequeEspecial());
                break;
            }
        };
        return limiteClienteMessage;
    }

    public List<LimiteParametro> getParametos() {
        List<LimiteParametro> parametros = new ArrayList<>();
        parametros.add(new LimiteParametro(0,1,0,0));
        parametros.add(new LimiteParametro(2,5,200,1000));
        parametros.add(new LimiteParametro(6,8,2000,2000));
        parametros.add(new LimiteParametro(9,9,15000,5000));
        return parametros;
    }
}
