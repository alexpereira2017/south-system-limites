package com.southsystem.limites.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;

@Entity
@AllArgsConstructor
@Data
public class LimiteParametro {
    private int escoreMinimo;
    private int escoreMaximo;
    private double limiteCartao;
    private double limiteChequeEspecial;
}
