package com.southsystem.limites.producer;

import com.southsystem.limites.dto.LimiteClienteMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitMQProducer {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    public void produce(LimiteClienteMessage limite){
        amqpTemplate.convertAndSend(exchange, routingkey, limite);
        log.info("Mensagem de limite enviada = " + limite);
    }
}
